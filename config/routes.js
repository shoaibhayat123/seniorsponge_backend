"use strict";

/**
 * Route Mappings
 *
 * Your routes map URLs to views and controllers
 */
const path = require('path');
const fs = require('fs');
var dir = path.join(__dirname, "../api/routes");

// Manually load controllers files
// var categories= require("../api/controllers/CategoryController"); // require entity controller name Categories


// else create separate directory and load auto

/**
 * @description loadModules read the routes directories and files to add routes dynamically {Not in use now}
 * @param callback
 */
function loadModules(callback) {
  var Entities = [];
  fs.readdir(dir, "utf8", function (err, files) {
    for (var f in files) {
      var obj = {};
      obj[path.basename(files[f], ".js")] = require(path.join(dir, path.basename(files[f], ".js")));
      Entities.push(obj);
    }
    var routes = {};
    for (var e in Entities) {
      for (var key in Entities[e]) {
        for (var route in Entities[e][key].routes) {
          routes[route] = Entities[e][key].routes[route];
        }
      }

    }
    callback(routes);
  });
}


/**
 * @desc routes end points object which is exporting to sails
 * @type {{routes: {get /users: module.exports.get, post /users: module.exports.add, delete /users: module.exports.destroy, put /users: module.exports.update}}}
 */
module.exports = {
  routes: {
    // Category routes
    // "get /category": 'CategoryController.get',
    // "post /category": "CategoryController.add",
    // "delete /category": "CategoryController.destroy",
    // "put /category": "CategoryController.update",
    "get /file/get": 'UploadController.get',
    "post /file": 'UploadController.postBy',
    "get /file/:key/:val": 'UploadController.getBy',
    "get /uploadfile" : 'UploadController.index',
    "post /file/upload" : 'UploadController.upload',
    "post /file/add" : 'UploadController.add',

    // Todos
    "get /todo": 'TodoController.get',
    "get /todo/:user_id": 'TodoController.getByUserId',
    "get /todo/:user_id/:complete": 'TodoController.getByUserTaskComplete',
    "get /todo/:complete": 'TodoController.getByTaskComplete',
    "post /todo": 'TodoController.add',
    "put /todo/:id": 'TodoController.updateById',
    "delete /todo/:id": 'TodoController.destroyById',
    "delete /todo": 'TodoController.destroyALL',

    //Email
    "/email/send" : 'EmailController.send',
    "/email/show" : 'EmailController.show',
    "post /email/inbox" : 'EmailController.inbox',
    "post /email/read" : 'EmailController.read',
    "/email/body" : 'EmailController.getBody',
    "post /email/newmails" : 'EmailController.incomingmails',
    "/email/mail" : 'EmailController.mail',

    // Users Type
    "get /usertype": 'UserTypeController.get',
    "post /usertype": 'UserTypeController.add',
    "put /usertype/:key/:val": 'UserTypeController.updateBy',
    "delete /usertype/:id": 'UserTypeController.destroyById',
    "delete /usertype/:key/:val": 'UserTypeController.destroyBy',
    "delete /usertype": 'UserTypeController.destroyALL',

    //  Users routes
    "get /user": 'UsersController.get',
    "get /user/:key/:val": 'UsersController.getBy',
    "get /users/:key/:val": 'UsersController.getByMany',
    "post /user": 'UsersController.add',
    "post /user/singin": 'UsersController.signin',
    "put /user/:key/:val": 'UsersController.updateBy',
    "put /user/:email": 'UsersController.updateByemail',
    "delete /user/:key/:val": 'UsersController.destroyBy',
    "delete /user": "UsersController.destroyALL",
    "post /user/album": "UsersController.addAlbum",
    "get /user/album": 'UsersController.getAlbums',
    'post /user/upload': 'UsersController.upload',
    'post /user/upload/image': 'UsersController.uploadImage',
    'post /user/album/image/add': 'UsersController.addImage',
    "get /user/album/images/:albumId": "UsersController.getAlbumImages",
    // Community Routes
    "get /community": "CommunityController.get",
    "post /community": "CommunityController.add",
    "delete /community": "CommunityController.destroyALL",
    "delete /community/:key/:val": 'CommunityController.destroyById',
    // "put /community": "CommunityController.put",
    "put /community/:key/:val": 'CommunityController.updateBy',

    // Events Routes
    "get /events": 'EventController.get',
    "post /events": 'EventController.add',
    "delete /events/:id": 'EventController.destroyById',
    "delete /events": 'EventController.destroyALL',
    "delete /events/:key/:val": 'EventController.destroyBy',
    "put /events/:key/:val": 'EventController.updateBy',

    // Community Type Routes
    "get /communitytype": 'CommunityTypeController.get',
    "post /communitytype" : 'CommunityTypeController.add',
    "delete /communitytype/:id" : 'CommunityTypeController.destroyById',
    "delete /communitytype" : 'CommunityTypeController.destroyALL',
    "delete /communitytype/:key/:val" : 'CommunityTypeController.destroyBy',
    "put /communitytype/:key/:val" : 'CommunityTypeController.updateBy',

    // Community Meta Routes
    "get /communitymeta": 'CommunityMetaController.get',
    "post /communitymeta" : 'CommunityMetaController.add',
    "delete /communitymeta/:id" : 'CommunityMetaController.destroyById',
    "delete /communitymeta" : 'CommunityMetaController.destroyALL',
    "delete /communitymeta/:key/:val" : 'CommunityMetaController.destroyBy',
    "put /communitymeta/:key/:val" : 'CommunityMetaController.updateBy',

    // Community Location Routes
    "get /communitylocation": 'CommunityLocationController.get',
    "post /communitylocation" : 'CommunityLocationController.add',
    "delete /communitylocation/:id" : 'CommunityLocationController.destroyById',
    "delete /communitylocation" : 'CommunityLocationController.destroyALL',
    "delete /communitylocation/:key/:val" : 'CommunityLocationController.destroyBy',
    "put /communitylocation/:key/:val" : 'CommunityLocationController.updateBy',

    // Classes Routes  --- not implement yet
    "get /classes": 'ClassesController.get',
    "post /classes": 'ClassesController.add',
    "delete /classes/:id": 'ClassesController.destroyById',
    "delete /classes": 'ClassesController.destroyALL',
    "delete /classes/:key/:val": 'ClassesController.destroyBy',
    "put /classes/:key/:val": 'ClassesController.updateBy',


    // File
    '/upload-file': {
      view: 'uploadfile'  // view 'uploadfile' in views directory will loaded automatically
    },
  }
};
