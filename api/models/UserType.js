"use strict";

/**
 * UsersType
 * @description :: Model for storing UsersType records
 */
var uuid = require('node-uuid');
module.exports = {
  schema: true,
  autoPK: false,
  attributes: {
    // Fill your attributes here
    id: {
      type: 'string',
      primaryKey: true,
      required: true,
      defaultsTo: function (){ return uuid.v4(); },
      index: true,
      uuidv4: true
    },
    name:{
      type:"string",
      required: true,
      unique: true
    },
    isActive:{
      type:"boolean",
      defaultsTo: true
    },
    isDelete:{
      type:"boolean",
      defaultsTo: false
    },
    toJSON() {
      return this.toObject();
    }
  },
  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
