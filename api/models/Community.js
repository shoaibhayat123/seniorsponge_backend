"use strict";

/**
 * Community
 * @description :: Model for storing Community records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,

    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      name: {
        type: "string",
        required: true,
        unique: true
      },
      address:{
        type:"string",
        required: true
      },
      lat: {
        type:"string",
        required: true
      },
      long: {
        type:"string",
        required: true
      },
      isActive:{
        type:"boolean",
        defaultsTo: false,
      },
      isDelete:{
        type:"boolean",
        defaultsTo: false
      },
      profileName: {
        type: "string",
        required: true
      },
      toJSON() {
        return this.toObject();
      }
    },

    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
