"use strict";

/**
 * Event
 * @description :: Model for storing Event records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      name:{
        type:"string",
        required: true
      },
      startDate: {
        type:"string",
        required: true,
        defaultsTo: function (){ return new Date().toDateString(); }
      },
      endDate: {
        type:"string",
        required: true,
        defaultsTo: function (){ return new Date().toDateString(); }
      },
      id_user: {
        collection: "user",
        via: "id"
      },
      days: {
        type:"array",
        required: true
      },
      community_id: {
        collection: "community",
        via: "id"
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
    beforeCreate: (values, next) => next()
};
