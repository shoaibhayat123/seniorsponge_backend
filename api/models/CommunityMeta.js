"use strict";

/**
 * CommunityMeta
 * @description :: Model for storing CommunityMeta records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      name:{
        type:"string",
        required: true,
        unique: true
      },
      community_id: {
        collection: "community",
        via: "id"
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};


