"use strict";

/**
 * Todos
 * @description :: Model for storing Classes records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      title:{
        type:"string",
        required: true
      },
      complete:{
        type: 'boolean',
        required: true
      },
      createdAt: {
        type:"string",
        required: true
      },
      user_id: {
        type:"string",
        required: true
      },
      id_user: {
        collection: "user",
        via: "id"
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};

