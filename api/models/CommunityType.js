"use strict";

/**
 * CommunityType
 * @description :: Model for storing CommunityType records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      id_userType: {
        collection: "usertype",
        via: "id"
      },
      id_community: {
        collection: "community",
        via: "id"
      },
      id_user: {
        collection: "user",
        via: "id"
      },
      isActive: {
        type:"boolean",
        defaultsTo: true
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};


