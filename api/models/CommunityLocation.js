"use strict";

/**
 * CommunityLocation
 * @description :: Model for storing CommunityLocation records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      name:{
        type:"string",
        required: true,
        unique: true
      },
      address:{
        type:"string",
        required: true
      },
      lat: {
        type:"string",
        required: true
      },
      long: {
        type:"string",
        required: true
      },
      isActive:{
        type:"boolean",
        defaultsTo: true
      },
      isDelete:{
        type:"boolean",
        defaultsTo: false
      },
      id_community: {
        collection: "community",
        via: "id"
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
