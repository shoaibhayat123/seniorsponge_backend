"use strict";

/**
 * FileUpload
 * @description :: Model for storing Classes records
 */
var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function (){ return uuid.v4(); },
        index: true,
        uuidv4: true
      },
      name:{
        type:"string",
        required: true,
        unique: true
      },
      path:{
        type: 'string',
        required: true
      },
      type: {
        type:"string",
        required: true
      },
      ext: {
        type:"string",
        required: true
      },
      status: {
        type:"string",
        required: true
      },
      size: {
        type:"string",
        required: true
      },
      id_user: {
        collection: "user",
        via: "id"
      },
      userId: {
        type: 'string',
        required: true,
      },
      JSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};

