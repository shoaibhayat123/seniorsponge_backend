"use strict";

/**
 * Users
 * @description :: Model for storing Users records
 */

var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function () {
          return uuid.v4();
        },
        index: true,
        uuidv4: true
      },
      albumId: {
        type: "string",
        required: true
      },
      Image: {
        type: "string",
        required: true
      },
      createdDate: {
        type: "string",
        required: true
      },
      createdBy: {
        type: "string",
        required: true
      },
      modifiedDate: {
        type: "string",
        required: true
      },
      isActive: {
        type: "boolean",
        defaultsTo: true
      },
      isDelete: {
        type: "boolean",
        defaultsTo: false
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
    beforeCreate:(values, next) =>next()
}
;
