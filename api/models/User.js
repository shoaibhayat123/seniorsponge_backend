"use strict";

/**
 * Users
 * @description :: Model for storing Users records
 */

var uuid = require('node-uuid');
module.exports = {
    schema: true,
    autoPK: false,
    attributes: {
      // Fill your attributes here
      id: {
        type: 'string',
        primaryKey: true,
        required: true,
        defaultsTo: function () {
          return uuid.v4();
        },
        index: true,
        uuidv4: true
      },
      firstName: {
        type: "string",
        required: true
      },
      lastName: {
        type: "string",
        required: true
      },
      createdDate: {
        type: "string",
        required: true
      },
      createdBy: {
        type: "string",
        required: true
      },
      modifiedDate: {
        type: "string",
        required: true
      },
      lat: {
        type: "string",
        required: true
      },
      long: {
        type: "string",
        required: true
      },
      isActive: {
        type: "boolean",
        defaultsTo: true
      },
      isDelete: {
        type: "boolean",
        defaultsTo: false
      },
      userName: {
        type: "string",
        required: true,
        unique: true
      },
      password: {
        type: "string",
        required: true
      },
      email: {
        type: "string",
        unique: true,
        required: true
      },
      age: {
        type: "integer",
        required: true
      },
      userTypeID: {
        type:"string",
        required: true
      },
      id_userType: {
        collection: "usertype",
        via: "id"
      },
      communityID: {
        type:"string",
        required: true
      },
      community_id: {
        collection: "community",
        via: "id"
      },
      toJSON() {
        return this.toObject();
      }
    },
    beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
