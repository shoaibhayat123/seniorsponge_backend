"use strict";

/**
 * CommunityTypeController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    CommunityType.find().populate('id_community').populate('id_userType').populate('id_user').then(function (type) {
      res.json(type);
    }).catch(function (err) {
      res.json(err);
    });
  },
  add: function (req, res) {
    console.log(req.body);
    var body = req.body;
    console.log(typeof body.type);

    if(body.id_community == undefined || body.id_community == '') {
      return res.status.json('Community ID Needed');
    }
    if(body.id_userType == undefined || body.id_userType == '') {
      return res.status.json('User Type Needed');
    }
    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    CommunityType.create(body).then(function (type) {
        CommunityType.find().populate('id_community').populate('id_userType').populate('id_user').then(function (type) {
          res.json(type);
        });
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyAll: function (req, res) {
    CommunityType.destroy().then(function () {
      res.json({
        status: true,
        message: "All Community Neta Data has been removed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request"
      });
    })
  },
  destroyById: function (req, res) {
    CommunityType.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Succesfully removed community type data"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request",
        error: err
      })
    })
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    filterBy[req.params.key] = req.params.val;
    CommunityType.destroy(filterBy).then(function () {
      res.json({
        status:true,
        message:"Community Type has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  updateBy: function (req, res) {
    var body = req.body;
    var filterBy = {};
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    if(body.id_community == undefined || body.id_community == '') {
      return res.status.json('Community ID Needed');
    }
    if(body.id_userType == undefined || body.id_userType == '') {
      return res.status.json('User Type Needed');
    }
    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    CommunityType.update(filterBy, body).then(function (communityType) {
      console.log('body_' , body);
      res.status(200).json(communityType);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  // updateBy: function (req, res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   CommunityType.find(filterBy).populate("id_community").then(function (type) {
  //     console.log(type);
  //     res.send(type);
  //   }).catch(function (err) {
  //     console.log(err);
  //     res.send(err);
  //   });
  //   return;
  //   CommunityType.update(filterBy, body).then(function (type) {
  //     res.status(200).json(type);
  //   }).catch(function (err) {
  //     res.status(400).json(err);
  //   });
  // }
};
