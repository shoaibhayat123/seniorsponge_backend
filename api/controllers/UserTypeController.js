"use strict";

/**
 * UserTypeController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req,res) {
    UserType.find().then(function (usertype) {
        res.json(usertype);
    }).catch(function (error) {
        res.json(error);
    })
  },
  add: function (req,res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    console.log(req.body.name);
    var body = req.body;
    UserType.create(body).then(function (type) {
      res.json(type);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyALL: function (req,res) {
    UserType.destroy().then(function () {
      res.json({
        status:true,
        message:"All UserType has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  destroyById: function (req, res) {
    UserType.destroy({id:req.params.id}).then(function () {
      res.status(200).json({
        status:true,
        message:"Successfully remove the usertype"
      });
    }).catch(function (error) {
      res.status(400).json({
        status:false,
        message:"Unable to process the request at this type because of the following error, Please fix the errors and request again! Thank you",
        error: error
      });
    })
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    UserType.destroy(filterBy).then(function () {
      res.json({
        status:true,
        message:"user type has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  updateBy: function (req, res) {
    console.log(req.body.name);
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    var body = req.body;
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    UserType.update(filterBy, body).then(function (usertype) {
      console.log('body_' , body);
      res.status(200).json(usertype);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  }
  // updateBy: function (req,res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   UserType.update(filterBy,body).then(function (usertype) {
  //       res.status(200).json(usertype);
  //   }).catch(function (error) {
  //       res.status(400).json(error);
  //   });
  //
  //   /*UserType.findOne(filterBy).populate("category").exec(function (err,type) {
  //     console.log(type);
  //       if(err){
  //         res.status(400).json(err);
  //       }else{
  //         for(var c=0 ;c < type.category.length; c++){
  //           console.log(c);
  //           type.category.remove(type.category[c].id);
  //         }
  //         type.save(function(err,data){
  //           if(err){
  //             console.log(err);
  //             res.send(err);
  //           }else{
  //             type.category.add(category);
  //             if(body.name){
  //               type.name = body.name;
  //             }
  //             if(type.isActive){
  //               type.isActive = body.isActive;
  //             }
  //             type.save(function(err,data){
  //               if(err){
  //                 console.log(err);
  //                 res.send(err);
  //               }else{
  //                 res.json(data);
  //               }
  //             });
  //           }
  //         });
  //       }
  //   });*/
  // }
};
