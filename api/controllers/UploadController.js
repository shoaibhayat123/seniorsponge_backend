"use strict";
var uuid = require('node-uuid');
var path = require('path');
/**
 * UploadController
 * @description :: Server-side logic for ...
 */

module.exports = {

  index: function (req,res){
    console.log('testing index');
    res.writeHead(200, {'content-type': 'text/html'});
    res.end(
      '<form action="http://localhost:3000/file/upload" enctype="multipart/form-data" method="post">'+
      '<input type="text" name="title"><br>'+
      '<input type="file" name="avatar" multiple="multiple"><br>'+
      '<input type="submit" value="Upload">'+
      '</form>'
    )
  },
  upload: function  (req, res) {
     var newFilename = req.file('avatar')._files[0].stream.filename;
    console.log('newFilename' + newFilename);
    var ext = (newFilename.lastIndexOf('.') + 1) > 0 ?
      newFilename.substr(newFilename.lastIndexOf('.') + 1) : "";
    console.log('ext' + ext);
    req.file('avatar').upload({
       saveAs: uuid.v4() + "." + ext,
        dirname: '/home/pc-09/Desktop/seniorsponge-api/assets' },
      function (err, files) {
      if (err) {
        console.log('testing error');
        return res.serverError(err);
      }
      return res.json({
        files: files
      });
    });
  },
  get:function (req,res) {
    FileUpload.find().populate('id_user').then(function (file) {
      res.json(file);
    }).catch(function (err) {
      res.json(err);
    })
  },
  getBy:function (req,res) {
    var filterBy = {};
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    FileUpload.find(filterBy).populate('id_user').then(function (file) {
      res.json(file);
    }).catch(function (err) {
      res.json(err);
    })
  },
  postBy:function (req,res) {
    FileUpload.find({type: req.body.type, userId: req.body.id_user}).populate('id_user', {where: {id: req.body.id_user}}).then(function (file) {
      res.json(file);
    }).catch(function (err) {
      res.json(err);
    })
  },
  add: function (req, res) {
    console.log(req.body);
    var body = req.body;
    console.log(typeof body.type);
    FileUpload.create(body).populate('id_user').then(function (file) {
      res.json(file);
      }).catch(function (err) {
      res.status(400).json(err);
    });
  }

};
