module.exports = {
  send: function(req, res) {
    if (req.body.email == undefined || req.body.email == "") {
      res.status(400).json('Bad Request');
    }
      else {
      var name = req.body.name == undefined  || req.body.name == "" ? "Anonymous"
        : req.body.name.toLowerCase().trim();
      var email = req.body.email.toLowerCase().trim();
      sails.hooks.email.send(
        "testEmail", {
          recipientName: name,
          senderName: "Shoaib"
        }, {
          to: email,
          subject: "Hi there"
        },
        function(err, message) {
          if (err) {
            return res.status(400).json({
              status: false,
              error: err
            });
          }
          return res.status(200).json(message);
        }
      );
    }
  },
  show: function(req, res) {
    var path = require('path');
    require('fs').readFile(path.resolve(__dirname,"..","..",".tmp","email.txt"), {encoding:"utf8"}, function(err, text) {
      if (err) {return res.serverError(err);}
      res.set("Content-Type","text/plain");
      return res.ok(text);
    });
  },

  // inbox: function(req, res) {
  //   if (req.body.email == undefined || req.body.email == "" || req.body.password == undefined
  //     || req.body.password == "" || req.body.servicetype == undefined
  //     || req.body.servicetype == "" || isNaN(obj.limit) || obj.limit == undefined) {
  //     res.status(400).json('Bad Request');
  //   }
  //   else {
  //     var inbox = require("inbox");
  //     var mailservice = req.body.servicetype.toLowerCase().trim();
  //     var email = req.body.email.toLowerCase().trim();
  //     var client = inbox.createConnection(false, "imap.gmail.com", {
  //       secureConnection: true,
  //       debug: true,
  //       auth: {
  //         user: email,
  //         pass: req.body.password
  //       }
  //     });
  //
  //     client.connect();
  //
  //     client.on("connect", function () {
  //       client.openMailbox("INBOX", function (err, info) {
  //         if (err) { return res.status(400).json({
  //           status: false,
  //           error: err
  //         });
  //         }
  //
  //         var limit = req.body.limit == "" ? 10 : req.body.limit;
  //         client.listMessages(-(limit), function (err, messages) {
  //           if (err) { return res.status(400).json({
  //             status: false,
  //             error: err
  //           });
  //           }
  //           messages.forEach(function (message) {
  //             console.log(message.UID + ": " + message.title);
  //           });
  //             res.status(200).json(messages);
  //         });
  //
  //       });
  //     });
  //
  //     client.on('error', function (err){
  //       console.log('Error', err);
  //       client.on('close', function (){
  //         console.log('DISCONNECTED!');
  //       });
  //       return res.status(400).json({
  //         status: false,
  //         error: err
  //       });
  //     });
  //
  //   }
  // },

  incomingmails: function(req, res) {
    var connect = require('../../config/email');
    var client = connect.connection(req.body);
    if (client == "Error") {
      return res.status(400).json('Bad Request');
    }
    else {

      client.connect();

      client.on("connect", function () {
        client.openMailbox("INBOX", function (err, info) {
          if (err) {
            return res.status(400).json({
              status: false,
              error: err
            });
          }
          client.on("new", function (message) {
            console.log(message.UID + ": " + message.title);
            res.status(200).json(message);
          });
        });
      });

      client.on('error', function (err){
        console.log('Error', err);
        return res.status(400).json({
          status: false,
          error: err
        });
      });
    }
  },


  mail: function (req, res) {

    var imaps = require('imap-simple');

    var config = {
      imap: {
        user: 'shoaibhayat93@gmail.com',
        password: 'hayat2222',
        host: 'imap.gmail.com',
        port: 993,
        tls: true,
        authTimeout: 3000
      }
    };

    imaps.connect(config).then(function (connection) {

      return connection.openBox('INBOX').then(function () {
        var searchCriteria = [
          'SEEN'
        ];

        var fetchOptions = {
          bodies: ['HEADER', 'TEXT'],
          markSeen: false
        };

        return connection.search(searchCriteria, fetchOptions).then(function (results) {
          var subjects = results.map(function (res) {
            return res.parts.filter(function (part) {
              return part;
                  // .which === 'HEADER';
            })[0].body.subject[0];
          });

          console.log(subjects);
          return res.status(200).json(subjects);
        });
      });
    });
  },


  read: function(req, res) {
    var connect = require('../../config/email');
    var client = connect.connection(req.body);
    if (client == "Error") {
      return res.status(400).json('Authentication Error');
    }
    else {
      client.connect();
      client.on("connect", function () {
        if (isNaN(req.body.UID) || req.body.UID == undefined || req.body.UID == "") {
          return res.status(400).json('UID Must Be Number');
        }
        var inbox = req.body.mailtype == '' || req.body.mailtype == undefined? 'INBOX' : req.body.mailtype;
        if(inbox.toUpperCase().trim() == 'INBOX') {
          client.openMailbox(inbox, function (err, info) {
            if (err) {
              return res.status(400).json({
                status: false,
                error: err
              });
            }
            if (req.body.seen.toLowerCase().trim() == 'unseen') {
              console.log('for reed');
              client.addFlags(req.body.UID, ["\\Seen"], function (err, flags) {
                if (err) {
                  return res.status(400).json({
                    status: false,
                    error: err + 'read'
                  });
                }
                console.log("Current flags for a message: ", flags);
                return res.status(200).json(flags);
              });
            }
            else if (req.body.seen.toLowerCase().trim() == 'seen') {
              console.log('for unread');
              client.removeFlags(req.body.UID, ["\\Seen"], function (err, flags) {
                if (err) {
                  return res.status(400).json({
                    status: false,
                    error: err + 'remove'
                  });
                }
                console.log("Current flags for a message: ", flags);
              });

              client.addFlags(req.body.UID, [], function (err, flags) {
                if (err) {
                  return res.status(400).json({
                    status: false,
                    error: err + 'unread'
                  });
                }
                console.log("Current flags for a message: ", flags);
                return res.status(200).json(flags);
              });
            }

            console.log('success');
          });
        }
      });

      client.on('error', function (err){
        console.log('Error', err);
        client.on('close', function (){
          console.log('DISCONNECTED!');
        });
        return res.status(400).json('Authentication OR Connection Error');
      });
    }
  },

  // getBody: function(req, res){
  //   var connect = require('../../config/email');
  //   var client = connect.connection(req.body);
  //   if (client == "Error") {
  //     return res.status(400).json('Authentication Error');
  //   }
  //   else {
  //     client.connect();
  //     client.on("connect", function () {
  //       if (isNaN(req.body.UID) || req.body.UID == undefined || req.body.UID == "") {
  //         return res.status(400).json('UID Must Be Number');
  //       }
  //       var inbox = req.body.mailtype == "" || req.body.mailtype == undefined ? "INBOX"
  //         : req.body.mailtype;
  //
  //       client.openMailbox(inbox, function (err, info) {
  //         if (err) {
  //           return res.status(400).json({
  //             status: false,
  //             error: err
  //           });
  //         }
  //           client.fetchData(req.body.UID, function(err, message) {
  //             if (err) {
  //               return res.status(400).json({
  //                 status: false,
  //                 error: err
  //               });
  //             }
  //             return res.status(200).json({message: message.bodystructure});
  //           });
  //       });
  //     });
  //   }
  // },

  // getBody: function(req, res) {
  //   var cache = [];
  //   var Imap = require("imap");
  //   var MailParser = require("mailparser").MailParser;
  //   // // var Promise = require("bluebird");
  //   // // Promise.longStackTraces();
  //
  //   var imapConfig = {
  //     user: req.body.email,
  //     password: req.body.password,
  //     host: req.body.servicetype,
  //     port: 993,
  //     tls: true
  //   };
  //
  //   var imap = new Imap(imapConfig);
  //   // // Promise.promisifyAll(imap);
  //
  //   imap.once("ready", execute);
  //   imap.once("error", function(err) {
  //     // log.error("Connection error: " +
  //     return res.status(400).json({
  //       status: false,
  //       error: err
  //     });
  //   });
  //
  //   imap.connect();
  //
  //   function execute() {
  //     var inbox = req.body.mailtype == "" || req.body.mailtype == undefined ? "INBOX"
  //       : req.body.mailtype;
  //     imap.openBox(inbox, false, function(err, mailBox) {
  //       if (err) {
  //         return res.status(400).json({
  //           status: false,
  //           error: err
  //         });
  //       }
  //       console.log('mailBox', mailBox);
  //       if (isNaN(req.body.UID) || req.body.UID == undefined || req.body.UID == "") {
  //         return res.status(400).json('UID Must Be Number');
  //       }
  //       console.log('req.body.UID', req.body.UID);
  //       // imap.search(["ALL"], function(err, results) {
  //       //   if(!results || !results.length){console.log("No Mail Of UID");imap.end();return res.status(200).json(
  //       //     "No Mail Of UID");}
  //         // mark as seen
  //       var results = req.body.UID;
  //          imap.setFlags(results, ['\\Seen'], function(err) {
  //          if (!err) {
  //          console.log("marked as read");
  //          } else {
  //            return res.status(400).json({
  //              status: false,
  //              error: err
  //            });
  //          }
  //          });
  //
  //         var f = imap.fetch(results, { bodies: "" });
  //         f.on("message", processMessage);
  //         f.once("error", function(err) {
  //           return res.status(400).json({
  //             status: false,
  //             error: err
  //           });
  //         });
  //         f.once("end", function() {
  //           console.log("Done fetching all unseen messages.");
  //           imap.end();
  //         });
  //       // });
  //     });
  //   }
  //
  //
  //   function processMessage(msg, seqno) {
  //     // console.log("Processing msg #" + seqno);
  //      // console.log('msg',msg);
  //
  //     var parser = new MailParser();
  //     parser.on("headers", function(headers) {
  //       console.log("Header: " + JSON.stringify(headers));
  //     });
  //
  //     parser.on('data', function(data) {
  //       console.log('email body data-----------', data);
  //       // return '';
  //       if (data.type === 'text') {
  //       // console.log('swq',seqno);
  //       // console.log('data.text',data.text);  /* data.html*/
  //         return res.status(200).json(data);
  //     }
  //
  //     if (data.type === 'attachment') {
  //         // console.log(data.filename);
  //         data.content.pipe(process.stdout);
  //         // console.log('data attachment', data);
  //         // data.content.on('end', () => data.release());
  //     }
  //
  //       // var cache = [];
  //       var body = JSON.stringify(data, function(key, value) {
  //         // console.log('value', value);
  //         if (typeof value === 'object' && value !== null) {
  //
  //           // console.log('cache.indexOf(value) !== -1', cache.indexOf(value) !== -1);
  //           if (cache.indexOf(value) !== -1) {
  //             // Circular reference found, discard key
  //             return;
  //           }
  //           // Store value in our collection
  //           cache.push(value);
  //         }
  //         return value;
  //       });
  //       // console.log('body',body);
  //       // console.log('cache',cache);
  //
  //       // if(cache.length > 0)
  //       // return res.status(200).send(body);
  //   });
  //
  //     msg.on("body", function(stream) {
  //       stream.on("data", function(chunk) {
  //         // console.log('chunk.toString("utf8")', chunk.toString("utf8"));
  //         parser.write(chunk.toString("utf8"));
  //       });
  //     });
  //     msg.once("end", function() {
  //       // console.log("Finished msg #" + seqno);
  //       parser.end();
  //     });
  //   }
  // },


  // getBody: function(req, res) {
  //   var Imap = require('imap');
  //   var inspect = require('util').inspect;
  //
  //   var imap = new Imap({
  //     user: req.body.email,
  //     password: req.body.password,
  //     host: req.body.servicetype,
  //     port: 993,
  //     tls: true
  //   });
  //
  //   imap.connect();
  //
  //   function openInbox(cb) {
  //     var inbox = req.body.mailtype == "" || req.body.mailtype == undefined ? "INBOX"
  //             : req.body.mailtype;
  //     imap.openBox(inbox, true, cb);
  //   }
  //
  //   imap.once('ready', function() {
  //     openInbox(function(err, box) {
  //       if (err) throw err;
  //       if (isNaN(req.body.UID) || req.body.UID == undefined || req.body.UID == "") {
  //                 return res.status(400).json('UID Must Be Number');
  //               }
  //               console.log('req.body.UID',req.body.UID);
  //       var f = imap.fetch(req.body.UID, {
  //         bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
  //         struct: true
  //       });
  //       f.on('message', function(msg, seqno) {
  //         console.log('Message #%d', seqno);
  //         var prefix = '(#' + seqno + ') ';
  //         msg.on('body', function(stream, info) {
  //           var buffer = '';
  //           stream.on('data', function(chunk) {
  //             buffer += chunk.toString('utf8');
  //           });
  //           stream.once('end', function() {
  //             console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
  //           });
  //           // return res.status(200).send(buffer);
  //         });
  //         msg.once('attributes', function(attrs) {
  //           console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
  //           return res.status(200).json(inspect(attrs, false, 8));
  //         });
  //         msg.once('end', function() {
  //           console.log(prefix + 'Finished');
  //         });
  //       });
  //       f.once('error', function(err) {
  //         console.log('Fetch error: ' + err);
  //       });
  //       f.once('end', function() {
  //         console.log('Done fetching all messages!');
  //         imap.end();
  //       });
  //     });
  //   });
  //
  //   imap.once('error', function(err) {
  //     console.log(err);
  //   });
  //
  //   imap.once('end', function() {
  //     console.log('Connection ended');
  //   });
  // },

  getBody: function(req, res) {
    var inspect = require('util').inspect;
    var fs      = require('fs');
    var base64  = require('base64-stream');
    var Imap    = require('imap');
    var arr = []; var size;
    var imap    = new Imap({
      user: req.body.email,
          password: req.body.password,
          host: req.body.servicetype,
      port: 993,
      tls: true
      //,debug: function(msg){console.log('imap:', msg);}
    });

    function toUpper(thing) { return thing && thing.toUpperCase ? thing.toUpperCase() : thing;}

    function findAttachmentParts(struct, attachments) {
      attachments = attachments ||  [];
      for (var i = 0, len = struct.length, r; i < len; ++i) {
        if (Array.isArray(struct[i])) {
          findAttachmentParts(struct[i], attachments);
        } else {
          if (struct[i].disposition && ['INLINE', 'ATTACHMENT'].indexOf(toUpper(struct[i].disposition.type)) > -1) {
            attachments.push(struct[i]);
          }
        }
      }
      return attachments;
    }

    function buildAttMessageFunction(attachment) {
      var filename = attachment.params.name;
      var encoding = attachment.encoding;

      return function (msg, seqno) {
        var prefix = '(#' + seqno + ') ';
        msg.on('body', function(stream, info) {
          //Create a write stream so that we can stream the attachment to file;
          console.log(prefix + 'Streaming this attachment to file', filename, info);
          var writeStream = fs.createWriteStream(filename);
          writeStream.on('finish', function() {
            console.log(prefix + 'Done writing to file %s', filename);
          });

          //stream.pipe(writeStream); this would write base64 data to the file.
          //so we decode during streaming using
          if (toUpper(encoding) === 'BASE64') {
            //the stream is base64 encoded, so here the stream is decode on the fly and piped to the write stream (file)
            stream.pipe(base64.decode()).pipe(writeStream);
            // arr.push(stream);
          } else  {
            //here we have none or some other decoding streamed directly to the file which renders it useless probably
            stream.pipe(writeStream);
            // arr.push(stream);
          }
          arr.push(writeStream);
          // console.log('arr' , arr);
          if(arr.length === size) {
            console.log('arr' , arr);
            return res.status(200).send(arr);
          }

        });
        msg.once('end', function() {
          console.log(prefix + 'Finished attachment %s', filename);
        });
      };
    }

    imap.once('ready', function() {
      imap.openBox('INBOX', true, function(err, box) {
        if (err) throw err;
        var f = imap.fetch(req.body.UID, {
          bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)'],
          struct: true
        });
        f.on('message', function (msg, seqno) {
          console.log('Message #%d', seqno);
          var prefix = '(#' + seqno + ') ';
          msg.on('body', function(stream, info) {
            var buffer = '';
            stream.on('data', function(chunk) {
              buffer += chunk.toString('utf8');
            });
            stream.once('end', function() {
              console.log(prefix + 'Parsed header: %s', Imap.parseHeader(buffer));
            });
          });
          msg.once('attributes', function(attrs) {
            var attachments = findAttachmentParts(attrs.struct);
            console.log(prefix + 'Has attachments: %d', attachments.length);
            size = attachments.length;
            for (var i = 0, len=attachments.length ; i < len; ++i) {
              var attachment = attachments[i];
              /*This is how each attachment looks like {
               partID: '2',
               type: 'application',
               subtype: 'octet-stream',
               params: { name: 'file-name.ext' },
               id: null,
               description: null,
               encoding: 'BASE64',
               size: 44952,
               md5: null,
               disposition: { type: 'ATTACHMENT', params: { filename: 'file-name.ext' } },
               language: null
               }
               */
              console.log(prefix + 'Fetching attachment %s', attachment.params.name);
              var f = imap.fetch(attrs.uid , { //do not use imap.seq.fetch here
                bodies: [attachment.partID],
                struct: true
              });
              //build function to process attachment message
              f.on('message', buildAttMessageFunction(attachment));
            }
          });
          msg.once('end', function() {
            console.log(prefix + 'Finished email');
          });
        });
        f.once('error', function(err) {
          console.log('Fetch error: ' + err);
        });
        f.once('end', function() {

          console.log('Done fetching all messages!');
          imap.end();
        });
      });
    });

    imap.once('error', function(err) {
      console.log(err);
    });

    imap.once('end', function() {
      console.log('Connection ended');
    });

    imap.connect();

  },

  inbox: function(req, res) {

    var arr = [];
    count = 0;
    var connect = require('../../config/email');
    var client = connect.connection(req.body);
    if (client == "Error") {
      return res.status(400).json('Bad Request');
    }
    else {
      console.log('inbox');
      client.connect();

        // [Gmail]/Starred
        // [Gmail]/Drafts
        // [Gmail]/Sent Mail
        // [Gmail]/Trash
        // INBOX

      var inbox = req.body.mailtype == "" || req.body.mailtype == undefined ? "INBOX"
        : req.body.mailtype;

      client.on("connect", function () {
        client.openMailbox(inbox, function (err, info) {
          if (err) {
            return res.status(400).json({
              status: false,
              error: err
            });
          }
          if(inbox == "INBOX" && req.body.seen != '' && req.body.seen != undefined) {
            var query = '';
            console.log('seen');
            if(req.body.seen.toLowerCase().trim() == 'unseen') {
              query = {unseen: true};
            }
            else if (req.body.seen.toLowerCase().trim() == 'seen') {
              query = {seen: true};
            }
            else {
              query = {or: {unseen: true, seen: true}};
            }
            client.search(query, function(err, UIDs){
              if (err) {
                return res.status(400).json({
                  status: false,
                  error: err
                });
              }
              console.log('UIDs', UIDs);
              UIDs.forEach(function (UID) {
                console.log('UID', UID);
                client.fetchData(UID, function (err, mail) {
                  if (err) {
                    return res.status(400).json({
                      status: false,
                      error: err
                    });
                  }
                  count++;
                  console.log('mail', mail);
                  arr.push(mail);
                  if(UIDs.length == count) {
                    return res.status(200).json(arr);
                  }
                });
              });
            });
          }
          else {
            var limit = req.body.limit == "" || req.body.limit == undefined ||
            isNaN(req.body.limit) ? 100000 : req.body.limit;
            client.listMessages(-(limit), function (err, messages) {
              if (err) {
                return res.status(400).json({
                  status: false,
                  error: err
                });
              }
              messages.forEach(function (message) {
                    console.log(message.UID + ": " + message.title);
              });
             return res.status(200).json(messages);
            });
          }
        });
      });

      client.on('error', function (err){
        console.log('Error', err);
        client.on('close', function (){
          console.log('DISCONNECTED!');
        });
        return res.status(400).json({
          status: false,
          error: err
        });
      });

    }
  }
};

module.exports.sendWelcomeMail = function(obj){
    sails.hooks.email.send(
      "testEmail", {
        recipientName: obj.userName,
        senderName: "Shoaib"
      }, {
        to: obj.email,
        subject: "Welcome to Senior Sponge"
      },
      function(err) {console.log(err || "Mail Sent!");}
    );
  }
