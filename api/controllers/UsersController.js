"use strict";

/**
 * UsersController
 * @description :: Server-side logic for ...
 */
// var yr_window = window;

var uuid = require('node-uuid');
var localStorage = require('localStorage');
module.exports = {
  get:function (req,res) {
    User.find().populate('id_userType').populate('community_id').then(function (model) {
      res.json(model);
    }).catch(function (err) {
      res.json(err);
    })
  },
  getBy: function (req, res) {
    var filterBy = {};
    req.params.val = req.params.key == "username" || req.params.key == "email" ? req.params.val.toLowerCase()
      : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);
    User.findOne(filterBy).populate('id_userType').populate('community_id').then(function (model) {
      res.json(model);
    }).catch(function (err) {
      res.json(err);
    })
  },
  getByMany: function (req, res) {
    var filterBy = {};
    req.params.val = req.params.key == "username" || req.params.key == "email" ? req.params.val.toLowerCase()
      : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);
    User.find(filterBy).populate('id_userType').populate('community_id').then(function (model) {
      res.json(model);
    }).catch(function (err) {
      res.json(err);
    })
  },
  add: function (req,res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    req.body.email = req.body.email == undefined ? req.body.email : req.body.email.toLowerCase();
    console.log(req.body);
    var body = req.body;
    var type = {};
    if(body.community_id == undefined || body.community_id == ''
    || body.communityID == '' || body.communityID == undefined) {
        return res.status.json('Community Type ID Needed');
    }
    if(body.id_userType == undefined || body.id_userType == ''
    || body.userTypeID == undefined || body.userTypeID == '') {
      return res.status.json('User Type ID Needed');
    }
    User.create(body).then(function (user) {
      if (user) {
        console.log('user');
        var emailServe = require('./EmailController');
        emailServe.sendWelcomeMail(user);
        User.find().populate('id_userType').populate('community_id').then(function (type) {
          console.log('usertype', type);
          res.status(200).json(type);
        });
      }
    }).catch(function (err) {
      res.status(400).json(err);
    });

  },
  destroy: function (req, res) {
    Categories.destroy().then(function () {
      res.json({
        status: true,
        message: "All users has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        error: err
      })
    })
  },
  destroyBy: function (req, res) {
    var filterBy = {};
    req.params.val = req.params.key == "username" || req.params.key == "email" ? req.params.val.toLowerCase()
      : req.params.val;filterBy[req.params.key] = req.params.val;
    User.destroy(filterBy).then(function () {
      res.json({
        status: true,
        message: "user has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        error: err
      })
    })
  },
  destroyALL: function (req, res) {
    User.destroy().then(function () {
      res.json({
        status: true,
        message: "All User has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        error: err
      })
    })
  },
  // updateBy: function (req, res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   User.find(filterBy).then(function (user) {
  //     console.log(user);
  //     res.send(user);
  //   }).catch(function (err) {
  //     console.log(err);
  //     res.send(err);
  //   });
  //   return;
  //   User.update(filterBy, body).then(function (user) {
  //     res.status(200).json(user);
  //   }).catch(function (err) {
  //     res.status(400).json(err);
  //   });
  // },
  updateByemail: function (req, res) {
    var filterBy = req.params.email.toLowerCase();
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    req.body.email = req.body.email == undefined ? req.body.email : req.body.email.toLowerCase();
    var body = req.body;
    console.log('body', body);

    if(body.community_id == undefined || body.community_id == ''
      || body.communityID == '' || body.communityID == undefined) {
      return res.status.json('Community ID Needed');
    }
    if(body.id_userType == undefined || body.id_userType == ''
      || body.userTypeID == undefined || body.userTypeID == '') {
      return res.status.json('User Type ID Needed');
    }

    User.update({email: filterBy}, body).then(function (user) {
      console.log('body_', body);
      res.status(200).json(user);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  updateBy: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    req.body.email = req.body.email == undefined ? req.body.email : req.body.email.toLowerCase();
    var body = req.body;
    var filterBy = {};
    req.params.val = req.params.key == "username" || req.params.key == "email" ? req.params.val.toLowerCase()
      : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    if(body.community_id == undefined || body.community_id == ''
      || body.communityID == '' || body.communityID == undefined) {
      return res.status.json('Community ID Needed');
    }
    if(body.id_userType == undefined || body.id_userType == ''
      || body.userTypeID == undefined || body.userTypeID == '') {
      return res.status.json('User Type ID Needed');
    }

    User.update(filterBy, body).then(function (user) {
      console.log('body_', body);
      res.status(200).json(user);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  signin: function (req, res) {
    var body = req.body;
    console.log("user signin request");
    console.log(body);
    User.find({
        email: body.email.toLowerCase(),
      password: body.password
    }).populate('id_userType').populate('community_id').then(function (model) {
      res.json(model);
    }).catch(function (err) {
      res.json(err);
    })
  },

  //Album Functions
  addAlbum: function (req, res) {
    var body = req.body;
    body.albumThumbnail = localStorage.getItem('albumThum');
    Album.create(body).then(function (user) {
      res.json(user);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  getAlbums: function (req, res) {
    Album.find().then(function (model) {
      res.json(model);
    }).catch(function (err) {
      res.json(err);
    })
  },

  //Album Thumbnail Image Upload
  upload: function (req, res) {
    var newFilename = req.file('album_Thumbnail')._files[0].stream.filename;
    console.log('newFilename' + newFilename);
    var ext = (newFilename.lastIndexOf('.') + 1) > 0 ? newFilename.substr(newFilename.lastIndexOf('.') + 1) : "";
    console.log('ext' + ext);
    localStorage.removeItem('albumThum');
    localStorage.setItem('albumThum', uuid.v4() + "." + ext);
    req.file('album_Thumbnail').upload({
      saveAs: localStorage.getItem('albumThum'),
      dirname: '/home/test-pc/localhost/www.seniorsponge.com/src/client/assets/uploads'
    }, function (err, files) {
      if (err) {
        console.log('testing error');
        return res.serverError(err);
      }
      return res.json({files: files});
    });
  },

  addImage: function (req, res) {
    var body = req.body;
    body.Image = localStorage.getItem('Image');
    Images.create(body).then(function (user) {
      res.json(user);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },

  getAlbumImages: function (req, res) {
    Images.findOne({
      albumId: req.params.albumId
    }).then(function (data) {
      console.log(data);
      res.json(data);
    }).catch(function (err) {
      console.log(err);
      res.json(err);
    });
  },

  //Album Thumbnail Image Upload
  uploadImage: function (req, res) {
    var newFilename = req.file('Image')._files[0].stream.filename;
    console.log('newFilename' + newFilename);
    var ext = (newFilename.lastIndexOf('.') + 1) > 0 ? newFilename.substr(newFilename.lastIndexOf('.') + 1) : "";
    console.log('ext' + ext);
    localStorage.removeItem('Image');
    localStorage.setItem('Image', uuid.v4() + "." + ext);
    req.file('Image').upload({
      saveAs: localStorage.getItem('Image'),
      dirname: '/home/test-pc/localhost/www.seniorsponge.com/src/client/assets/uploads'
    }, function (err, files) {
      if (err) {
        console.log('testing error');
        return res.serverError(err);
      }
      return res.json({files: files});
    });
  }
};
