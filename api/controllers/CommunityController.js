"use strict";

/**
 * CommunityController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    Community.find().then(function (community) {
      res.json(community);
    }).catch(function (error) {
      res.json(error);
    })
  },
  add: function (req,res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    console.log(req.body);
    var body = req.body;
    Community.create(body).then(function (community) {
      res.json(community);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyALL: function (req, res) {
    Community.destroy().then(function () {
      res.json({
        status: true,
        message: "All Communities has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        error: err
      })
    })
  },
  destroyById: function (req, res) {
    Community.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Successfully remove the community"
      });
    }).catch(function (error) {
      res.status(400).json({
        status: false,
        message: "Unable to process the request at this type because of the following error, Please fix the errors and request again! Thank you",
        error: error
      });
    }/**
     * Created by pc-09 on 7/12/17.
     */)
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    Community.destroy(filterBy).then(function () {
      res.json({
        status:true,
        message:"Community has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  updateBy: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);
    var body = req.body;
    Community.find(filterBy).populate("type").then(function (community) {
      console.log(community);
      res.send(community);
    }).catch(function (error) {
      console.log(error);
      res.send(error);
    });
    return;
    Community.update(filterBy, body).then(function (community) {
      res.status(200).json(community);
    }).catch(function (error) {
      res.status(400).json(error);
    });

    // UserType.findOne(filterBy).populate("category").exec(function (err, type) {
    //   console.log(type);
    //   if (err) {
    //     res.status(400).json(err);
    //   } else {
    //     for (var c = 0; c < type.category.length; c++) {
    //       console.log(c);
    //       type.category.remove(type.category[c].id);
    //     }
    //     type.save(function (err, data) {
    //       if (err) {
    //         console.log(err);
    //         res.send(err);
    //       } else {
    //         type.category.add(category);
    //         if (body.name) {
    //           type.name = body.name;
    //         }
    //         if (type.isActive) {
    //           type.isActive = body.isActive;
    //         }
    //         type.save(function (err, data) {
    //           if (err) {
    //             console.log(err);
    //             res.send(err);
    //           } else {
    //             res.json(data);
    //           }
    //         });
    //       }
    //     });
    //   }
    // });
  },
  dummy: function (req, res) {
    res.send({status: true, message: "User has beed added successfully :P ", body: req.body});
  }
};
