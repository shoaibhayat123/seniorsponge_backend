"use strict";

/**
 * CommunityMetaController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    CommunityMeta.find().populate('community_id').then(function (meta) {
      res.json(meta);
    }).catch(function (err) {
      res.json(err);
    });
  },
  add: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    console.log(req.body);
    var body = req.body;
    console.log(typeof body.type);

    if(body.community_id == undefined || body.community_id == '') {
      return res.status.json('Community ID Needed');
    }

    CommunityMeta.create(body).then(function (meta) {
      CommunityMeta.find().populate('community_id').then(function (meta) {
        res.json(meta);
      });
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyAll: function (req, res) {
    CommunityMeta.destroy().then(function () {
      res.json({
        status: true,
        message: "All Community Neta Data has been removed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request"
      });
    })
  },
  destroyById: function (req, res) {
    CommunityMeta.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Succesfully removed Meta data"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request",
        error: err
      })
    })
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    CommunityMeta.destroy(filterBy).then(function () {
      res.json({
        status:true,
        message:"Community Meta has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  // updateBy: function (req, res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   CommunityMeta.find(filterBy).populate("community_id").then(function (meta) {
  //     console.log(meta);
  //     res.send(meta);
  //   }).catch(function (err) {
  //     console.log(err);
  //     res.send(err);
  //   });
  //   return;
  //   CommunityMeta.update(filterBy, body).then(function (meta) {
  //     res.status(200).json(meta);
  //   }).catch(function (err) {
  //     res.status(400).json(err);
  //   });
  // }
  updateBy: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    var body = req.body;
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    if(body.community_id == undefined || body.community_id == '') {
      return res.status.json('Community ID Needed');
    }

    CommunityMeta.update(filterBy, body).then(function (communityMeta) {
      console.log('body_' , body);
      res.status(200).json(communityMeta);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  }
};
