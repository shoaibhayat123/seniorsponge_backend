"use strict";

/**
 * TodoController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    Todo.find().populate('id_user').then(function (todo) {
      res.json(todo);
    }).catch(function (err) {
      res.json(err);
    });
  },
  getByUserId: function (req, res) {
    var filterBy = {user_id: req.params.user_id};;
    Todo.find(filterBy).populate('id_user').then(function (todo) {
      res.json(todo);
    }).catch(function (err) {
      res.json(err);
    });
  },
  getByUserTaskComplete: function (req, res) {
    var filterBy = {user_id: req.params.user_id, complete: req.params.complete};;
    Todo.find(filterBy).populate('id_user').then(function (todo) {
      res.json(todo);
    }).catch(function (err) {
      res.json(err);
    });
  },
  getByTaskComplete: function (req, res) {
    var filterBy = {complete: req.params.complete};;
    Todo.find(filterBy).populate('id_user').then(function (todo) {
      res.json(todo);
    }).catch(function (err) {
      res.json(err);
    });
  },
  add: function (req, res) {
    req.body.title = req.body.title == undefined || req.body.title == '' ? 'Task Title Not Defined' : req.body.title;
    req.body.complete = req.body.complete == undefined || req.body.complete == '' ? false : req.body.complete;
    req.body.createdAt = new Date().toLocaleString();
    console.log(req.body);
    var body = req.body;
    console.log(typeof body.type);

    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    Todo.create(body).then(function (todo) {
      Todo.find(todo.id).populate('id_user').then(function (todo) {
        // Classes.find(classes.id).populate('id_user').then(function (classes) {
        res.json(todo);
        // })
      })
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyAll: function (req, res) {
    Todo.destroy().then(function () {
      res.json({
        status: true,
        message: "All Todo's has been removed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request"
      });
    });
  },
  destroyById: function (req, res) {
    Todo.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Succesfully Removed Todo Task"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request",
        error: err
      });
    });
  },
  updateById: function (req, res) {
    var filterBy = {id: req.params.id};
    var body = req.body;
    console.log(typeof body.type);

    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    Todo.update(filterBy, body).then(function (todo)
    {
      res.json({
        status:true,
        message:"Todo has been Updated"
      });

    }).catch(function (err) {
      res.status(400).json(err);
    });
  }
};
