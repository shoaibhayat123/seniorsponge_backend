"use strict";

/**
 * CommunityLocationController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    CommunityLocation.find().populate('id_community').then(function (location) {
      res.json(location);
    }).catch(function (err) {
      res.json(err);
    });
  },
  add: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    console.log(req.body);
    var body = req.body;
    console.log(typeof body.type);

    if(body.id_community == undefined || body.id_community == '') {
      return res.status.json('Community ID Needed');
    }

    CommunityLocation.create(body).then(function (location) {
      CommunityLocation.find().populate('id_community').then(function (location) {
        res.json(location);
      });
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyAll: function (req, res) {
    CommunityLocation.destroy().then(function () {
      res.json({
        status: true,
        message: "All Community Neta Data has been removed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request"
      });
    });
  },
  destroyById: function (req, res) {
    CommunityLocation.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Succesfully removed location data"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request",
        error: err
      });
    });
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    CommunityLocation.destroy(filterBy).then(function () {
      res.json({
        status:true,
        message:"Community Location has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  // updateBy: function (req, res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   CommunityLocation.find(filterBy).populate("id_community").then(function (location) {
  //     console.log(location);
  //     res.send(location);
  //   }).catch(function (err) {
  //     console.log(err);
  //     res.send(err);
  //   });
  //   return;
  //   CommunityLocation.update(filterBy, body).then(function (location) {
  //     res.status(200).json(location);
  //   }).catch(function (err) {
  //     res.status(400).json(err);
  //   });
  // }
  updateBy: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    var body = req.body;
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    if(body.id_community == undefined || body.id_community == '') {
      return res.status.json('Community ID Needed');
    }

    CommunityLocation.update(filterBy, body).then(function (communityLocation) {
      console.log('body_' , body);
      res.status(200).json(communityLocation);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  }
};
