"use strict";

/**
 * EventController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    Events.find().populate('community_id').populate('id_user').then(function (event) {
      res.json(event);
    }).catch(function (err) {
      res.json(err);
    })
  },
  add: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    console.log(req.body);
    var body = req.body;

    if(body.community_id == undefined || body.community_id == '') {
      return res.status.json('Community ID Needed');
    }
    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    Events.create(body).then(function (event) {
      Events.find(event.id).populate('community_id').populate('id_user').then(function (event) {
        res.json(event);
      });
    }).catch(function (err) {
      res.status(400).json(err);
    })
  },
  destroyAll: function (req, res) {
    Events.destroy().then(function () {
      res.json({
        status: true,
        message: "All Events has been Destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to Process to delete request",
        error: err
      });
    })
  },
  destroyById: function (req, res) {
    Events.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Succefully remove the event"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unalbe to Process to delete request",
        error: err
      });
    })
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    Events.destroy(filterBy).then(function () {
      res.json({
        status:true ,
        message:"Event has been destroyed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  updateBy: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    var body = req.body;
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    console.log(filterBy);

    if(body.community_id == undefined || body.community_id == '') {
      return res.status.json('Community ID Needed');
    }
    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    Events.update(filterBy, body).then(function (events) {
      console.log('body_' , body);
      res.status(200).json(events);
    }).catch(function (err) {
      res.status(400).json(err);
    });
  }
  // updateBy: function (req, res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   Events.find(filterBy).populate("community_id").then(function (event) {
  //     console.log(event);
  //     res.send(event);
  //   }).catch(function (err) {
  //     console.log(err);
  //     res.send(err);
  //   });
  //   return;
  //   Events.update(filterBy, body).then(function (event) {
  //     res.status(200).json(event);
  //   }).catch(function (err) {
  //     res.status(400).json(err);
  //   });
  //
  // }
};
