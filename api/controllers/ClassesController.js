"use strict";

/**
 * ClassesController
 * @description :: Server-side logic for ...
 */

module.exports = {
  get: function (req, res) {
    Classes.find().populate('community_id').populate('id_user').then(function (classes) {
      res.json(classes);
    }).catch(function (err) {
      res.json(err);
    });
  },
  add: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    console.log(req.body);
    var body = req.body;
    console.log(typeof body.type);

    if(body.community_id == undefined || body.community_id == '') {
      return res.status.json('Community ID Needed');
    }
    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    Classes.create(body).then(function (classes) {
      Classes.find(classes.id).populate('community_id').populate('id_user').then(function (classes) {
        // Classes.find(classes.id).populate('id_user').then(function (classes) {
          res.json(classes);
        // })
      })
    }).catch(function (err) {
      res.status(400).json(err);
    });
  },
  destroyAll: function (req, res) {
    Classes.destroy().then(function () {
      res.json({
        status: true,
        message: "All Classes has been removed"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request"
      });
    });
  },
  destroyById: function (req, res) {
    Classes.destroy({id: req.params.id}).then(function () {
      res.status(200).json({
        status: true,
        message: "Succesfully removed classes data"
      });
    }).catch(function (err) {
      res.status(400).json({
        status: false,
        message: "Unable to process the delete request",
        error: err
      });
    });
  },
  destroyBy: function (req,res) {
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;
    Classes.destroy(filterBy).then(function (classes) {
        res.json({
          status:true,
          message:"Class has been destroyed"
         });
    }).catch(function (err) {
      res.status(400).json({
        status:false,
        error: err
      })
    })
  },
  // updateBy: function (req, res) {
  //   console.log(req.body);
  //   var filterBy = {};
  //   filterBy[req.params.key] = req.params.val;
  //   console.log(filterBy);
  //   var body = req.body;
  //   Classes.find(filterBy).populate("community_id").populate('id_user').then(function (classes) {
  //     console.log(classes);
  //     res.send(classes);
  //   }).catch(function (err) {
  //     console.log(err);
  //     res.send(err);
  //   });
  //   return;
  //   Classes.update(filterBy, body).then(function (classes) {
  //     res.status(200).json(classes);
  //   }).catch(function (err) {
  //     res.status(400).json(err);
  //   });
  // }
  updateBy: function (req, res) {
    req.body.name = req.body.name == undefined ? req.body.name : req.body.name.toLowerCase();
    var body = req.body;
    var filterBy = {};
    req.params.val = req.params.key == "name" ? req.params.val.toLowerCase() : req.params.val;
    filterBy[req.params.key] = req.params.val;

    if(body.community_id == undefined || body.community_id == '') {
      return res.status.json('Community ID Needed');
    }
    if(body.id_user == undefined || body.id_user == '') {
      return res.status.json('User ID Needed');
    }

    Classes.update(filterBy, body).then(function (classes) {// Classes.find(classes.id).populate('id_user').then(function (classes) {
        res.json({
          status:true,
          message:"Class has been Updated"
        });

    }).catch(function (err) {
      res.status(400).json(err);
    });
  }
};
