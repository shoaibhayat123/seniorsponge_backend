/**
 * Created by maizy on 4/12/17.
 */


var categories= require("../controllers/CategoriesController"); // require entity controller name Categories
module.exports = {
  routes: {
    "get /users": categories.get,
    "post /users": categories.add,
    "delete /users": categories.destroy,
    "put /users": categories.update
  }
};
